import React, { Component } from "react";
import * as firebase from 'firebase';
import { Text, View } from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack'


import HomeScreen from './components/auth/Home'
import RegisterScreen from './components/auth/Register'
import LoginScreen from './components/auth/Login'
import MainScreen from './components/Main'
import SaveScreen from './components/main/Save'
import AddScreen from './components/main/Add'
import CommentScreen from './components/main/Comment'

//redux
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './redux/reducers'
import thunk from 'redux-thunk'
const store = createStore(rootReducer, applyMiddleware(thunk))

//Firebase Init en mode Dev
const firebaseConfig = {
  apiKey: "AIzaSyDH-YyrxLuVVtgktgpurZqBn0X1xs3a_O0",
  authDomain: "instagram-native-7cd7a.firebaseapp.com",
  databaseURL: 'https://instagram-native-7cd7a.firebaseio.com',
  projectId: "instagram-native-7cd7a",
  storageBucket: "instagram-native-7cd7a.appspot.com",
  messagingSenderId: "354496328129",
  appId: "1:354496328129:web:0421f990286fdfcd66ff40"
};
if(firebase.apps.length === 0){
    firebase.initializeApp(firebaseConfig);
}

const Stack = createStackNavigator();

//AuthListener to check if user or not, utilisé une classe mais pas une fonction pour gérer les states(méthodes sans hooks)
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        }
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged((user) => {
            if(!user) {
                this.setState({
                    loggedIn: false,
                    loaded: true,
                })
            } else {
                this.setState({
                    loggedIn: true,
                    loaded: true,
                })
            }
        })
    }

    render() {
        const { loggedIn, loaded} = this.state;
        if(!loaded) {
            return(
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <Text>Loading...</Text>
                </View>
            )
        }
        if(!loggedIn) {
            return (
                <NavigationContainer>
                    <Stack.Navigator initialRouteName="Home">
                        <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
                        <Stack.Screen name="Register" component={RegisterScreen} />
                        <Stack.Screen name="Login" component={LoginScreen} />
                    </Stack.Navigator>
                </NavigationContainer>
            );
        }
        return(
            //Routes after authentification
            <Provider store={store}>
                <NavigationContainer >
                <Stack.Navigator initialRouteName="Main">
                    <Stack.Screen name="Main" component={MainScreen} />
                    <Stack.Screen name="Add" component={AddScreen} navigation={this.props.navigation}/>
                    <Stack.Screen name="Save" component={SaveScreen} navigation={this.props.navigation}/>
                    <Stack.Screen name="Comment" component={CommentScreen} navigation={this.props.navigation}/>
                </Stack.Navigator>
                </NavigationContainer>
            </Provider>
                
            )
    }
}

export default App;
